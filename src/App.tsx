import './App.css';
import Location from './Location';
import Search from './Search';

function App() {
  return (
    <div className="App">
      <Location />
      <Search />
    </div>
  );
}

export default App;
