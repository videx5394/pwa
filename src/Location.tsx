import gql from 'graphql-tag';
import React from 'react';

const SEND_LOCATION = gql`
  query SEND_LOCATION($input: SearchInput!) {
    search(input: $input) {
      items {
        ... on Product {
          id
          name
        }
      }
      aggregations {
        field
      }
    }
  }
`;
const getLocation = (callBack: any) => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(callBack);
  }
};

export default function Location() {
  const handleClick = () => {
    getLocation(postion => {
      console.log(postion);
    });
  };

  return (
    <div>
      <button onClick={handleClick}>location</button>
    </div>
  );
}
