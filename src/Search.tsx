import { useLazyQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import React from 'react';
import { useDebounce } from 'react-use';

const GET_PRODUCTS_LIST_BY_SEARCH = gql`
  query SearchedProductLIst($input: SearchInput!) {
    search(input: $input) {
      items {
        ... on Product {
          id
          name
        }
      }
      aggregations {
        field
      }
    }
  }
`;

export default function Search() {
  const [searchedValue, setSearchedValue] = React.useState('');
  const [getProductsList, { loading, data }] = useLazyQuery(GET_PRODUCTS_LIST_BY_SEARCH);

  console.log(loading, data);

  useDebounce(
    () => {
      console.log(searchedValue, 'searchedValue');
      if (searchedValue !== '')
        getProductsList({
          variables: {
            input: {
              term: searchedValue
            }
          }
        });
    },
    100,
    [searchedValue]
  );

  return (
    <input
      type="text"
      onChange={e => {
        setSearchedValue(e.target.value);
      }}
    />
  );
}
